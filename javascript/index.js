//#TableOfContents
//-global
//-qualifying
//-race

//////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------//
//#global-----------------------------------------------------------//
//------------------------------------------------------------------//
var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

var maxCarSpeed = -1; //per tick
var maxCarJerk = -1; //Jerk is related to throttle; //unit = Jerk/Throttle
var friction = 1; //friction is the rate of deceleration

var currentSpeed = 0;

var carData = new Array();

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

//Join the server
client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: "2O3N4nCIW8P2Wg"
    }
  });
});

//Speed = (prevSpeed * friction) + jerk

function send(json) 
{
	if(messageSentThisGameTick == false)
	{
		messageSentThisGameTick = true;
		client.write(JSON.stringify(json));
		return client.write('\n');
	}
};

function setupCarInfo(cars)
{
	//if we've already setup, then don't do it again
	if(carData.length != 0) return;
	
	for(var i = 0; i < cars.length; i++)
	{
		var car = cars[i];
		
		var name = car.id.name;
		var color = car.id.color;
		
		var length = car.dimensions.length;
		var width = car.dimensions.width;
		var guideFlagPosition = car.dimensions.guideFlagPosition;
		
		carData[i] = new Object();
		carData[i].name = name;
		carData[i].color = color;
		carData[i].length = length;
		carData[i].width = width;
		carData[i].guideFlagPosition = guideFlagPosition;
		carData[i].speed = 0.0;
		carData[i].prevSpeed = 0.0;
		carData[i].acceleration = 0.0;
		carData[i].prevAcceleration = 0.0;
		carData[i].jerk = 0.0;
		carData[i].prevJerk = 0.0;
		carData[i].lastPieceIndex = -1;
		carData[i].lastInPieceDistance = 0;
		carData[i].stoppingDistance = 0;
		carData[i].slipAngle = 0;
		carData[i].prevSlipAngle = 0;
	}
}


var myCar;
var currentGameTick = -1;
var messageSentThisGameTick = false;

var firstTick = true;
var two = false;

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) 
{
	if(data.gameTick != currentGameTick)
	{
		currentGameTick = data.gameTick;
		messageSentThisGameTick = false;
		alreadyLoggedForThisTick = false;
	}
	
	if(q_qualifyingActive)
	{
		q_handleStreamForQualifying(data);
		return;
	}
	else
	{
		r_handleStreamForRace(data);
	}
});

function ping()
{
	send(
			{
				msgType: "ping",
				"gameTick": currentGameTick
			}
		);
}

function throttle(throttle)
{
	send(
			{
				"msgType": "throttle",
				"data": throttle,
				"gameTick": currentGameTick
			}
		);
}

var alreadyLoggedForThisTick = false;
function log(message)
{
	if(alreadyLoggedForThisTick)
	{
		console.log(padRight("", 9, '.') + ":   " + message);
	}
	else //Haven't logged yet
	{
		alreadyLoggedForThisTick = true;
		console.log(padRight("", 79, '-'));
		console.log(padRight(currentGameTick, 9, '.') + ":   " + message);
	}
	
}

function padRight(str, length, padChar)
{
	str = String(str);
	
	var padAmount = length - str.length;
	for(var i = 0; i < padAmount; i++)
	{
		str += padChar;
	}

	return (str);
}

var trackData = new Object();
trackData.switches = new Array();
trackData.longestStraight = new Object();
trackData.longestStraight.startIndex = -1;
trackData.longestStraight.endIndex = -1;
trackData.longestStraight.length = -1;

function handleGameInit(data)
{
	
	handleGameInitPieces(data.race.track.pieces);
	
	trackData.lanes = data.race.track.lanes;
	
	setupCarInfo(data.race.cars)
	
	//log(trackData.lanes);
	//log(trackData.switches);
}

function handleGameInitPieces(pieces)
{
	var lastBendIndex = 0;
	var newBend = false;
	var inABend = false;
	var currentBendAngle = 0;
	
	//save off all of the track pieces
	trackData.pieces = pieces;
	
	//get all of the switches
	for(var i = 0; i < pieces.length; i++)
	{
		var piece = pieces[i];
		
		//store all of our switch pieces in an object
		if (piece.hasOwnProperty("switch")) 
		{
			if(piece["switch"] == true)
			{
				var temp = new Object();
				temp["pieceIndex"] = i;
				trackData.switches.push(temp);
			}
		}
	}
	
	//set inside lane
	for(var i = 0; i < pieces.length; i++)
	{
		var piece = pieces[i];
		
		//TODO: handle end of pieces
		if (piece.hasOwnProperty("radius") && piece.hasOwnProperty("angle")) 
		{
			trackData.pieces[i]["type"] = "bend";
			
			//if we weren't in a bend before
			if(!inABend)
			{
				//we're in a new bend now
				newBend = true;
				inABend = true;
				currentBendAngle = piece.angle;
			}
			else //we've been in a bend
			{
				//if we were bending right
				if(currentBendAngle > 0)
				{
					//if we're now bending left
					if(piece.angle < 0)
					{
						newBend = true;
					}
				}
				//if we were bending left
				else if (currentBendAngle < 0)
				{
					//if we're now bending right
					if(piece.angle > 0)
					{
						newBend = true;
					}
				}
				
				//store off the new current bend angle
				currentBendAngle = piece.angle;
			}
			
			
			var insideLane;
			
			//right turn
			if(piece.angle > 0)
			{
				insideLane = "Right";
			}
			else //left turn
			{
				insideLane = "Left";
			}
			
			//if we just entered this bend
			if(newBend)
			{				
				//loop through all of our pieces since the last bend
				for(var j = lastBendIndex + 1; j <= i; j++)
				{
					//set their inside lane
					trackData.pieces[j]["inside lane"] = insideLane;
				}
				
				lastBendIndex = i;
				newBend = false;
			}
			else //we're just continuing a bend we were in
			{
				trackData.pieces[i]["inside lane"] = insideLane;
				lastBendIndex = i;
			}
		}
		else if(inABend) //aka a bend just ended in a straight
		{
			//handle bend ending
			inABend = false;
			newBend = false;
		}
		
		if (!piece.hasOwnProperty("radius") && !piece.hasOwnProperty("angle")) 
		{
			piece["type"] = "straight";
		}
	}
	
	var lastPieceStraight = false;
	var potentialLongestStraightStartIndex = 0;
	var potentialLongestStraightEndIndex = 0;
	var potentialLongestStraightDistance = 0;
	for(var i = 0; i < trackData.pieces.length; i++)
	{
		var piece = trackData.pieces[i];
		
		//TODO: handle end of pieces
		if (piece.hasOwnProperty("length")) 
		{
			trackData.pieces[i]["type"] = "straight";
			
			//if the last piece wasn't straight
			if(!lastPieceStraight)
			{
				potentialLongestStraightStartIndex = i;
			}
			
			potentialLongestStraightDistance += piece.length;
			
			lastPieceStraight = true;
		}
		else if(lastPieceStraight) //if we just ended a straight
		{
			lastPieceStraight = false;
			potentialLongestStraightEndIndex = i;
			if(potentialLongestStraightDistance > trackData.longestStraight.length)
			{
				trackData.longestStraight.startIndex = potentialLongestStraightStartIndex;
				trackData.longestStraight.endIndex = potentialLongestStraightEndIndex;
				trackData.longestStraight.length = potentialLongestStraightDistance;
			}
			
			potentialLongestStraightDistance = 0;
		}
	}
	
	//if we had a straight run into the finish line
	if(lastPieceStraight)
	{
		for(var i = 0; i < trackData.pieces.length; i++)
		{
			if(trackData.pieces[i]["type"] == "straight")
			{
				potentialLongestStraightDistance += trackData.pieces[i].length;
			}
			else
			{
				lastPieceStraight = false;
				
				potentialLongestStraightEndIndex = i;
				
				if(potentialLongestStraightDistance > trackData.longestStraight.length)
				{
					trackData.longestStraight.startIndex = potentialLongestStraightStartIndex;
					trackData.longestStraight.endIndex = potentialLongestStraightEndIndex;
					trackData.longestStraight.length = potentialLongestStraightDistance;
				}
				
				potentialLongestStraightDistance = 0;
				break;
			}
		}
	}
	
	//console.log(trackData.longestStraight);
	
	if(true) //just for scope
	{
		//handle the last section of the track
		var insideLane;
		
		//if the first piece wasn't a bend
		if(trackData.pieces[0]["inside lane"] == null)
		{
			//use the second piece
			insideLane = trackData.pieces[1]["inside lane"];
			trackData.pieces[0]["inside lane"] = insideLane;
		}
		else //if the first piece was a bend
		{
			insideLane = trackData.pieces[0]["inside lane"];
		}
		
		var i = trackData.pieces.length - 1;
		while(trackData.pieces[i]["inside lane"] == null && i > 0)
		{
			trackData.pieces[i]["inside lane"] = insideLane;
			i--;
		}
	}
}

function getCarSpeeds(data) //data is carPositions data
{
	for(var i = 0; i < data.length; i++)
	{
		var distanceTraveled = 0;
		var carName = data[i].id.name;
		var carColor = data[i].id.color;
		
		var slipAngle = data[i].angle;
		
		var piecePosition = data[i].piecePosition;
		var inPieceDistance = piecePosition.inPieceDistance;
		var pieceIndex = piecePosition.pieceIndex;
		var piece = trackData.pieces[pieceIndex];
		//if we're on a new piece
		if(pieceIndex != carData[i].lastPieceIndex)
		{
			if(carData[i].lastPieceIndex != -1)
			{
				distanceTraveled = (piece.length - carData[i].lastInPieceDistance) + inPieceDistance;
			}
			else
			{
				distanceTraveled = inPieceDistance - carData[i].lastInPieceDistance;
			}
		}
		else
		{
			distanceTraveled = inPieceDistance - carData[i].lastInPieceDistance;
		}
		
		if(distanceTraveled > maxCarSpeed)
		{
			//if this is our first new max speed
			if(maxCarSpeed == -1)
			{
				//then the distance we've traveled is the jerk
				maxCarJerk = distanceTraveled;
			}
			
			maxCarSpeed = distanceTraveled;
			log("New Max Speed: " + maxCarSpeed);
		}
		
		//store acceleration and speed
		carData[i].prevSpeed = carData[i].speed;
		carData[i].prevAcceleration = carData[i].acceleration;
		
		carData[i].prevSlipAngle = carData[i].angle;
		
		carData[i].speed = distanceTraveled;
		carData[i].acceleration = carData[i].speed - carData[i].prevSpeed;
		carData[i].slipAngle = slipAngle;
		
		log("Speed: " + carData[i].speed); 
		log("Acceleration: " + carData[i].acceleration); 
		log("Angle: " + carData[i].slipAngle); 
	}//end finding top speed
}

function getStoppingDistances(data) //data is carPositions data
{
	//should figure out what piece that would put me on
	for(var i = 0; i < data.length; i++)
	{
		var speed = carData[i].speed;
		carData[i].stoppingDistance = 0;
		
		while(speed > .00000001)
		{
			//Speed = (prevSpeed * friction) + jerk
			speed *= friction;
			carData[i].stoppingDistance += speed;
		}
		
		log("Stopping distance: " + carData[i].stoppingDistance);
	}
}

//------------------------------------------------------------------//
//#globalEND--------------------------------------------------------//
//------------------------------------------------------------------//
//////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------//
//#qualifying-------------------------------------------------------//
//------------------------------------------------------------------//
var q_qualifyingActive = true;

var q_LongestStraightStartTick = -1;
var q_LongestStraightActive = false;
var q_LongestStraightCompleted = false;

function q_handleStreamForQualifying(data)
{
	//TODO:
	//Switch all of these to qualifying
	switch(data.msgType)
	{
		case "carPositions":			
			q_handleCarPositions(data.data);
		break;
		
		case "join":
			log("Joined Race")
		break;
		
		case "yourCar":
			myCar = data.data;
		break;
		
		case "gameInit":
			handleGameInit(data.data);
		break;
		
		case "gameStart":
			log("Race has started");
		break;
		
		case "lapFinished":
			log("Someone finished a lap!");
		break;
		
		case "gameEnd":
			log("Race has ended");
		break;
		
		default:
			log("Unhandled Event: " + data.msgType);
			log(data);
		break;
	}
	
	ping();
}

var q_State = "initialize";

function q_handleCarPositions(data)
{
	getCarSpeeds(data);
	
	switch(q_State)
	{
		case "initialize":
			q_handleCarPositions_initialize(data);
		break;
		case "qualifying":
			q_handleCarPositions_qualifying(data);
		break;
	}
	
	//Need to not do this now
	for(var i = 0; i < data.length; i++)
	{
		var carName = data[i].id.name;
		var carColor = data[i].id.color;
		var slipAngle = data[i].angle;
		var piecePosition = data[i].piecePosition;
		
		//zero based index of the piece the car is on
		var pieceIndex = piecePosition.pieceIndex;
		var piece = trackData.pieces[pieceIndex];
		
		//the distance the car's Guide Flag (see above) has travelled from the start of the piece along the current lane
		var inPieceDistance = piecePosition.inPieceDistance;//zero based index of the piece the car is on
		
		//a pair of lane indices. Usually startLaneIndex and endLaneIndex are equal, but they do differ when the car is currently switching lane
		var lane = piecePosition.lane;
		
		var startLaneIndex = lane.startLaneIndex;
		var endLaneIndex = lane.endLaneIndex;
		
		//The number 0 indicates that the car is on its first lap. The number -1 indicates that it has not yet crossed the start line to begin it's first lap.
		var lap = data[i].lap;
		
		//used to handle gunning during the longest straight
		//	to find out our max speed
		if(carName == myCar.name && carColor == myCar.color)
		{
			if(carData[i].lastPieceIndex != pieceIndex)
			{
				//need to handle when there are no straights
				if(!q_LongestStraightCompleted && pieceIndex == trackData.longestStraight.startIndex)
				{
					q_LongestStraightActive = true;
					q_LongestStraightStartTick = currentGameTick;
					
					//need to pull current speed first
					throttle(1);
					log("Longest Straight Sampling Activated");
				}
				else if(q_LongestStraightActive && pieceIndex == trackData.longestStraight.endIndex)
				{
					//get distance travelled
					q_LongestStraightActive = false;
					q_LongestStraightCompleted = true;
					
					throttle(.5);
					log("Longest Straight Sampling Completed");
				}
			}
		}
	
	}
	
	
	for(var i = 0; i < data.length; i++)
	{
		var carName = data[i].id.name;
		var carColor = data[i].id.color;
		var slipAngle = data[i].angle;
		var piecePosition = data[i].piecePosition;
		
		//zero based index of the piece the car is on
		var pieceIndex = piecePosition.pieceIndex;
		var piece = trackData.pieces[pieceIndex];
		
		//the distance the car's Guide Flag (see above) has travelled from the start of the piece along the current lane
		var inPieceDistance = piecePosition.inPieceDistance;//zero based index of the piece the car is on
		
		//a pair of lane indices. Usually startLaneIndex and endLaneIndex are equal, but they do differ when the car is currently switching lane
		var lane = piecePosition.lane;
		
		var startLaneIndex = lane.startLaneIndex;
		var endLaneIndex = lane.endLaneIndex;
		
		//The number 0 indicates that the car is on its first lap. The number -1 indicates that it has not yet crossed the start line to begin it's first lap.
		var lap = data[i].lap;

		if(carName == myCar.name && carColor == myCar.color)
		{
			carData[i].lastInPieceDistance = inPieceDistance;
			var isBend = false;
			if(carData[i].lastPieceIndex != pieceIndex)
			{
				if (piece.hasOwnProperty("radius") && piece.hasOwnProperty("angle")) 
				{
					isBend = true;
				}
				else
				{
					isBend = false;
				}
				
				carData[i].lastPieceIndex = pieceIndex;
				var nextSwitchIndex = getNextSwitchIndex(pieceIndex);
				
				var indexToSendSwitchOn = nextSwitchIndex - 1;
				if(indexToSendSwitchOn < 0)
				{
					indexToSendSwitchOn =  trackData.pieces.length - 1;
				}
				
				log("Currently on piece " + pieceIndex + ".  Next switch at " + nextSwitchIndex);
				
				//if we're on a switch and we haven't started switching yet
				if(indexToSendSwitchOn == pieceIndex && startLaneIndex == endLaneIndex)
				{
					//figure out what the most popular inside lane is
					//	from this switch until the next switch?
					
					//Don't count straight aways as having inside lanes.
					//	Should only count curves.
					
					var subsequentNextSwitchIndex = getNextSwitchIndex(nextSwitchIndex + 1);
					
					var laneDistances = new Array();
					
					for(var j = 0; j < trackData.lanes.length; j++)
					{
						laneDistances[j] = 0;
					}
					
					
					//if we don't cross the finish line between switches
					if(nextSwitchIndex < subsequentNextSwitchIndex)
					{
						//should I include the subsequentNextSwitchIndex?
						//does that piece matter?
						for(var j = nextSwitchIndex; j < subsequentNextSwitchIndex; j++)
						{
							var piece = trackData.pieces[j];
							
							if(piece.hasOwnProperty("radius") && piece.hasOwnProperty("angle"))
							{
								//loop through all of the lanes
								for(var k = 0; k < trackData.lanes.length; k++)
								{
									var arcLength;
									
									//a positive value tells you lane is to right of the center
									//s negative value means left of center
									
									//right turn
									if(piece.angle > 0)
									{
										arcLength = Math.abs(piece.angle) * (Math.PI/180) * (piece.radius - trackData.lanes[k].distanceFromCenter);
									}
									else //left turn
									{
										arcLength = Math.abs(piece.angle) * (Math.PI/180) * (piece.radius + trackData.lanes[k].distanceFromCenter);
									}
									laneDistances[k] += arcLength;
								}
							}
						}
					}
					else //we cross the finish line between switches
					{
						//loop from nextSwitch to finish line
						for(var j = nextSwitchIndex; j < trackData.pieces.length; j++)
						{
							var piece = trackData.pieces[j];
							
							if(piece.hasOwnProperty("radius") && piece.hasOwnProperty("angle"))
							{
								//loop through all of the lanes
								for(var k = 0; k < trackData.lanes.length; k++)
								{
									var arcLength;
									
									//a positive value tells you lane is to right of the center
									//s negative value means left of center
									
									//right turn
									if(piece.angle > 0)
									{
										arcLength = Math.abs(piece.angle) * (Math.PI/180) * (piece.radius - trackData.lanes[k].distanceFromCenter);
									}
									else //left turn
									{
										arcLength = Math.abs(piece.angle) * (Math.PI/180) * (piece.radius + trackData.lanes[k].distanceFromCenter);
									}
									laneDistances[k] += arcLength;
								}
							}
						}
						
						//loop from finish line to subsequentNextSwitchIndex
						for(var j = 0; j < subsequentNextSwitchIndex; j++)
						{
							var piece = trackData.pieces[j];
							
							if(piece.hasOwnProperty("radius") && piece.hasOwnProperty("angle"))
							{
								//loop through all of the lanes
								for(var k = 0; k < trackData.lanes.length; k++)
								{
									var arcLength;
									
									//a positive value tells you lane is to right of the center
									//s negative value means left of center
									
									//right turn
									if(piece.angle > 0)
									{
										arcLength = Math.abs(piece.angle) * (Math.PI/180) * (piece.radius - trackData.lanes[k].distanceFromCenter);
									}
									else //left turn
									{
										arcLength = Math.abs(piece.angle) * (Math.PI/180) * (piece.radius + trackData.lanes[k].distanceFromCenter);
									}
									laneDistances[k] += arcLength;
								}
							}
						}
					}
					

					
					var shortestLaneIndex = 0;
					var shortestLaneDistance = Number.MAX_VALUE;
					for(var j = 0; j < laneDistances.length; j++)
					{
						//if we have a shorter distance
						if(laneDistances[j] < shortestLaneDistance)
						{
							//store the distance and the index
							shortestLaneDistance = laneDistances[j];
							shortestLaneIndex = j;
						}
					}
					
					//should probably use a while loop for this as it could go on for awhile
					for(var j = 0; j < laneDistances.length; j++)
					{
						//if we have a lane that has the same distance as the shortest
						if(laneDistances[j] == shortestLaneDistance && j != shortestLaneIndex)
						{
							//evaluate that lane to see which would be optimal
							//according to the next switch
							
							//stay in our lane if it matches the shortest
							//shouldn't switch unless necessary because switches are costly
							if(j == startLaneIndex)
							{
								shortestLaneIndex = j;
								break;
							}
						}
					}
					
					//Will need to make the lanes prioritized in case an opponent is in the way
					
					//If we're not on the best lane, then switch
					if(startLaneIndex != shortestLaneIndex)
					{
						//if the shortest lane is to the left
						if(shortestLaneIndex < startLaneIndex)
						{
							log("On lane " + startLaneIndex + " switch left");
							send({"msgType": "switchLane", "data": "Left", "gameTick": currentGameTick});
						}
						else if(shortestLaneIndex > startLaneIndex) //shortest lane is to the right
						{
							log("On lane " + startLaneIndex + " switch right");
							send({"msgType": "switchLane", "data": "Right", "gameTick": currentGameTick});
						}
						else
						{
							log("WHAT!? " + startLaneIndex + "=?=" + shortestLaneIndex);
						}
					}
					else
					{
						log("On lane " + startLaneIndex + " no switch");
					}
				}
				else if(startLaneIndex != endLaneIndex)
				{
					log("making switch");
				}
				
				//only a single switch can per car can occur on the same track piece at a time, regardless of the number of lanes on the track.
			}
		}
	}
}

function q_handleCarPositions_initialize(data)
{
	if(currentGameTick == 1)
	{
		//set max throttle so we can get the jerk
		throttle(1);
		return;
	}
	else if(currentGameTick == 2)
	{
		//get max jerk
		for(var i = 0; i < data.length; i++)
		{
			var carName = data[i].id.name;
			var carColor = data[i].id.color;
			if(carName == myCar.name && carColor == myCar.color)
			{
				//get max Jerk
				q_getMaxJerk(i);
				break;
			}
		}
		throttle(0);
	}
	else if(currentGameTick == 3)
	{
		//get friction
		for(var i = 0; i < data.length; i++)
		{
			var carName = data[i].id.name;
			var carColor = data[i].id.color;
			if(carName == myCar.name && carColor == myCar.color)
			{
				//get friction
				q_getFriction(i);
				break;
			}
		}
		throttle(1);
		q_State = "qualifying";
	}
}

function q_handleCarPositions_qualifying(data)
{
	getStoppingDistances(data);
	throttle(1);
}

function q_getMaxJerk(carIndex)
{
	//then the distance we've traveled is the jerk
	maxCarJerk = carData[carIndex].speed;
	log("Max Jerk: " + maxCarJerk);
}

function q_getFriction(carIndex)
{
	//then the distance we've traveled is the jerk
	friction = carData[carIndex].speed / carData[carIndex].prevSpeed;
	log("Friction: " + friction);
}


//------------------------------------------------------------------//
//#qualifyingEND----------------------------------------------------//
//------------------------------------------------------------------//
//////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------//
//#race-------------------------------------------------------------//
//------------------------------------------------------------------//

function r_handleStreamForRace(data)
{
	switch(data.msgType)
	{
		case "carPositions":			
			handleCarPositions(data.data);
		break;
		
		case "join":
			log("Joined Race")
		break;
		
		case "yourCar":
			myCar = data.data;
		break;
		
		case "gameInit":
			handleGameInit(data.data);
		break;
		
		case "gameStart":
			log("Race has started");
		break;
		
		case "lapFinished":
			log("Someone finished a lap!");
		break;
		
		case "gameEnd":
			log("Race has ended");
		break;
		
		default:
			log("Unhandled Event: " + data.msgType);
			log(data);
		break;
	}
	
	ping();
}


//------------------------------------------------------------------//
//#raceEND----------------------------------------------------------//
//------------------------------------------------------------------//
//////////////////////////////////////////////////////////////////////


function handleCarPositions(data)
{	
	getCarSpeeds(data);	
	
	for(var i = 0; i < data.length; i++)
	{
		var carName = data[i].id.name;
		var carColor = data[i].id.color;
		var slipAngle = data[i].angle;
		var piecePosition = data[i].piecePosition;
		
		//zero based index of the piece the car is on
		var pieceIndex = piecePosition.pieceIndex;
		var piece = trackData.pieces[pieceIndex];
		
		//the distance the car's Guide Flag (see above) has travelled from the start of the piece along the current lane
		var inPieceDistance = piecePosition.inPieceDistance;//zero based index of the piece the car is on
		
		//a pair of lane indices. Usually startLaneIndex and endLaneIndex are equal, but they do differ when the car is currently switching lane
		var lane = piecePosition.lane;
		
		var startLaneIndex = lane.startLaneIndex;
		var endLaneIndex = lane.endLaneIndex;
		
		//The number 0 indicates that the car is on its first lap. The number -1 indicates that it has not yet crossed the start line to begin it's first lap.
		var lap = data[i].lap;
		
		if(carName == myCar.name && carColor == myCar.color)
		{
			var isBend = false;
			if(carData[i].lastPieceIndex != pieceIndex)
			{
				if (piece.hasOwnProperty("radius") && piece.hasOwnProperty("angle")) 
				{
					isBend = true;
				}
				else
				{
					isBend = false;
				}
				
				carData[i].lastPieceIndex = pieceIndex;
				var nextSwitchIndex = getNextSwitchIndex(pieceIndex);
				
				var indexToSendSwitchOn = nextSwitchIndex - 1;
				if(indexToSendSwitchOn < 0)
				{
					indexToSendSwitchOn =  trackData.pieces.length - 1;
				}
				
				log("Currently on piece " + pieceIndex + ".  Next switch at " + nextSwitchIndex);
				
				//if we're on a switch and we haven't started switching yet
				if(indexToSendSwitchOn == pieceIndex && startLaneIndex == endLaneIndex)
				{
					//figure out what the most popular inside lane is
					//	from this switch until the next switch?
					
					//Don't count straight aways as having inside lanes.
					//	Should only count curves.
					
					var subsequentNextSwitchIndex = getNextSwitchIndex(nextSwitchIndex + 1);
					
					var laneDistances = new Array();
					
					for(var j = 0; j < trackData.lanes.length; j++)
					{
						laneDistances[j] = 0;
					}
					
					
					//if we don't cross the finish line between switches
					if(nextSwitchIndex < subsequentNextSwitchIndex)
					{
						//should I include the subsequentNextSwitchIndex?
						//does that piece matter?
						for(var j = nextSwitchIndex; j < subsequentNextSwitchIndex; j++)
						{
							var piece = trackData.pieces[j];
							
							if(piece.hasOwnProperty("radius") && piece.hasOwnProperty("angle"))
							{
								//loop through all of the lanes
								for(var k = 0; k < trackData.lanes.length; k++)
								{
									var arcLength;
									
									//a positive value tells you lane is to right of the center
									//s negative value means left of center
									
									//right turn
									if(piece.angle > 0)
									{
										arcLength = Math.abs(piece.angle) * (Math.PI/180) * (piece.radius - trackData.lanes[k].distanceFromCenter);
									}
									else //left turn
									{
										arcLength = Math.abs(piece.angle) * (Math.PI/180) * (piece.radius + trackData.lanes[k].distanceFromCenter);
									}
									laneDistances[k] += arcLength;
								}
							}
						}
					}
					else //we cross the finish line between switches
					{
						//loop from nextSwitch to finish line
						for(var j = nextSwitchIndex; j < trackData.pieces.length; j++)
						{
							var piece = trackData.pieces[j];
							
							if(piece.hasOwnProperty("radius") && piece.hasOwnProperty("angle"))
							{
								//loop through all of the lanes
								for(var k = 0; k < trackData.lanes.length; k++)
								{
									var arcLength;
									
									//a positive value tells you lane is to right of the center
									//s negative value means left of center
									
									//right turn
									if(piece.angle > 0)
									{
										arcLength = Math.abs(piece.angle) * (Math.PI/180) * (piece.radius - trackData.lanes[k].distanceFromCenter);
									}
									else //left turn
									{
										arcLength = Math.abs(piece.angle) * (Math.PI/180) * (piece.radius + trackData.lanes[k].distanceFromCenter);
									}
									laneDistances[k] += arcLength;
								}
							}
						}
						
						//loop from finish line to subsequentNextSwitchIndex
						for(var j = 0; j < subsequentNextSwitchIndex; j++)
						{
							var piece = trackData.pieces[j];
							
							if(piece.hasOwnProperty("radius") && piece.hasOwnProperty("angle"))
							{
								//loop through all of the lanes
								for(var k = 0; k < trackData.lanes.length; k++)
								{
									var arcLength;
									
									//a positive value tells you lane is to right of the center
									//s negative value means left of center
									
									//right turn
									if(piece.angle > 0)
									{
										arcLength = Math.abs(piece.angle) * (Math.PI/180) * (piece.radius - trackData.lanes[k].distanceFromCenter);
									}
									else //left turn
									{
										arcLength = Math.abs(piece.angle) * (Math.PI/180) * (piece.radius + trackData.lanes[k].distanceFromCenter);
									}
									laneDistances[k] += arcLength;
								}
							}
						}
					}
					

					
					var shortestLaneIndex = 0;
					var shortestLaneDistance = Number.MAX_VALUE;
					for(var j = 0; j < laneDistances.length; j++)
					{
						//if we have a shorter distance
						if(laneDistances[j] < shortestLaneDistance)
						{
							//store the distance and the index
							shortestLaneDistance = laneDistances[j];
							shortestLaneIndex = j;
						}
					}
					
					//should probably use a while loop for this as it could go on for awhile
					for(var j = 0; j < laneDistances.length; j++)
					{
						//if we have a lane that has the same distance as the shortest
						if(laneDistances[j] == shortestLaneDistance && j != shortestLaneIndex)
						{
							//evaluate that lane to see which would be optimal
							//according to the next switch
							
							//stay in our lane if it matches the shortest
							//shouldn't switch unless necessary because switches are costly
							if(j == startLaneIndex)
							{
								shortestLaneIndex = j;
								break;
							}
						}
					}
					
					//Will need to make the lanes prioritized in case an opponent is in the way
					
					//If we're not on the best lane, then switch
					if(startLaneIndex != shortestLaneIndex)
					{
						//if the shortest lane is to the left
						if(shortestLaneIndex < startLaneIndex)
						{
							log("On lane " + startLaneIndex + " switch left");
							send({"msgType": "switchLane", "data": "Left", "gameTick": currentGameTick});
						}
						else if(shortestLaneIndex > startLaneIndex) //shortest lane is to the right
						{
							log("On lane " + startLaneIndex + " switch right");
							send({"msgType": "switchLane", "data": "Right", "gameTick": currentGameTick});
						}
						else
						{
							log("WHAT!? " + startLaneIndex + "=?=" + shortestLaneIndex);
						}
					}
					else
					{
						log("On lane " + startLaneIndex + " no switch");
					}
				}
				else if(startLaneIndex != endLaneIndex)
				{
					log("making switch");
				}
				
				//only a single switch can per car can occur on the same track piece at a time, regardless of the number of lanes on the track.
			}
		}
	}
}

function getNextSwitchIndex(currentPieceIndex)
{
	for(var i = 0; i < trackData.switches.length; i++)
	{		
		if(trackData.switches[i]["pieceIndex"] >= currentPieceIndex)
		{
			return(trackData.switches[i]["pieceIndex"]);
		}
	}
	
	//if it makes it here, then the next switch is the first switch in the list
	return(trackData.switches[0]["pieceIndex"]);
}



jsonStream.on('error', function() {
  return console.log("disconnected");
});
